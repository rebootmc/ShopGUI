package com.rebootmc.shopgui.util;

import org.bukkit.ChatColor;

import java.text.NumberFormat;

/**
 * Created by Matthew on 25/02/2015.
 */
public class TextUtil {

    /**
     * Returns the friendly name (ie. no _'s for enumerates)
     *
     * @param name The name to be formatted
     * @return The formatted friendly name
     */
    public static String getFriendlyName(String name) {
        StringBuilder ret = new StringBuilder();
        String[] replace = name.split("_");

        for (String segment : replace) {
            ret.append(segment.substring(0, 1).toUpperCase() + segment.substring(1).toLowerCase() + " ");
        }

        return ret.toString().trim();
    }

    /**
     * Returns a formatted String representation of a double, rounded to 2
     * decimal places
     *
     * @param value The double to be formatted
     * @return A formatted string of the double
     */
    public static String formatDouble(double value) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        return nf.format(value);
    }

    /**
     * Returns a color-formatted String of the input
     *
     * @param text The input string
     * @return A formatted String
     */
    public static String color(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}

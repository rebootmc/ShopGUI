package com.rebootmc.shopgui.util;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 25/02/2015.
 */
public class ItemUtil {

    /**
     * Creates an ItemStack with specified parameters
     *
     * @param material The material of the item
     * @param name     The name of the item
     * @param amount   The number of items in this stack
     * @param lore     The lore for the item
     * @return The created ItemStack object
     */
    public static ItemStack createItemStack(Material material, int amount, String name, List<String> lore) {
        return createItemStack(material, amount, (short) 0, name, lore);
    }

    /**
     * Creates an ItemStack with specified parameters
     *
     * @param material   The material of the item
     * @param name       The name of the item
     * @param amount     The number of items in this stack
     * @param durability The durability of the item
     * @param lore       The lore for the item
     * @return The created ItemStack object
     */
    public static ItemStack createItemStack(Material material, int amount, short durability, String name, List<String> lore) {
        ItemStack ret = new ItemStack(material, amount);
        ret.setDurability(durability);
        ItemMeta meta = ret.getItemMeta();

        if (name != null) {
            meta.setDisplayName(TextUtil.color(name));
        } else if (material == Material.MOB_SPAWNER) {
            meta.setDisplayName(TextUtil.color("&r" + TextUtil.getFriendlyName(EntityType.fromId(ret.getDurability()).toString()) + " Spawner"));
        }

        if (lore != null && !lore.isEmpty()) {
            List<String> coloredLore = new ArrayList<String>();
            for (String piece : lore) {
                coloredLore.add(TextUtil.color(piece));
            }
            meta.setLore(coloredLore);
        }

        ret.setItemMeta(meta);
        return ret;
    }


    /**
     * Returns if an inventory has enough of an item
     *
     * @param inventory The inventory to check
     * @param item      The item to find
     * @param amount    The number of items searching for
     * @return True if the inventory has enough of the item, false otherwise
     */
    public static boolean has(Inventory inventory, ItemStack item, int amount) {
        int count = 0;
        for (ItemStack is : inventory.getContents()) {
            if (is != null) {
                if (is.isSimilar(item)) {
                    if (is.getAmount() >= amount) {
                        return true;
                    } else {
                        count += is.getAmount();
                    }
                }
            }
        }
        if (count >= amount) {
            return true;
        }
        return false;
    }

    /**
     * Gives an item to a player, drops any stacks if needed
     *
     * @param player The player to receive the item
     * @param item   The item to be given
     * @param amount The amount of copies of the itemstack to give
     * @return Whether or not any items were dropped
     */
    public static boolean giveItem(Player player, ItemStack item, int amount) {
        World world = player.getWorld();
        ItemStack itemstack = item.clone();
        ItemStack[] split = new ItemStack[1];
        boolean dropped = false;
        int maxsize = itemstack.getMaxStackSize();
        int stacks = amount / maxsize;
        int remaining = amount % maxsize;

        itemstack.setAmount(amount);

        if (amount > maxsize) {
            split = new ItemStack[stacks + (remaining > 0 ? 1 : 0)];
            // ie. 70 stack can only be 64
            for (int i = 0 ; i < stacks ; i++) {
                ItemStack maxStackSize = itemstack.clone();
                maxStackSize.setAmount(maxsize);
                split[i] = maxStackSize;
            }
            if (remaining > 0) {
                ItemStack remainder = itemstack.clone();
                remainder.setAmount(remaining);
                split[stacks] = remainder;
            }
        } else {
            split[0] = itemstack;
        }

        for (ItemStack itm : split) {
            if (itm != null) {
                // Check their inventory space
                if (hasSpace(player.getInventory(), itm)) {
                    player.getInventory().addItem(itm);
                } else {
                    world.dropItem(player.getLocation(), itm);
                    dropped = true;
                }
            }
        }

        return dropped;
    }

    /**
     * Checks if an inventory can fit a split itemstack
     *
     * @param inventory The inventory to check
     * @param itemstack The item being put into the inventory
     * @return True if the inventory can fit the item, false otherwise
     */
    public static boolean hasSpace(Inventory inventory, ItemStack itemstack) {
        int totalFree = 0;
        for (ItemStack is : inventory.getContents()) {
            if (is == null) {
                totalFree += itemstack.getMaxStackSize();
            } else if (is.isSimilar(itemstack)) {
                totalFree += is.getAmount() > itemstack.getMaxStackSize() ? 0 : itemstack.getMaxStackSize() - is.getAmount();
            }
        }
        return totalFree >= itemstack.getAmount();
    }

    /**
     * Clears an ItemStack ItemMeta
     *
     * @param itemstack The ItemStack to be cleared
     * @return The ItemStack with no ItemMeta
     */
    public static ItemStack clearItemMeta(ItemStack itemstack) {
        itemstack.setItemMeta(null);
        return itemstack;
    }

    /**
     * Sets an ItemStacks lore to a list of strings
     *
     * @param itemstack The ItemStack
     * @param lore      The lore
     */
    public static void setLore(ItemStack itemstack, List<String> lore) {
        ItemMeta meta = itemstack.getItemMeta();
        meta.setLore(lore);
        itemstack.setItemMeta(meta);
    }
}

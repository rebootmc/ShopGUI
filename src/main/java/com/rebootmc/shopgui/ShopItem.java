package com.rebootmc.shopgui;

import org.bukkit.Material;

import java.util.List;

public class ShopItem {

    private Material material;
    private int data, amount, donatorAmount;
    private List<String> names;
    private double buy, sell, donatorBuy, donatorSell;

    public ShopItem(Material material, int data, List<String> names) {
        this.material = material;
        this.data = data;
        this.names = names;
    }

    public Material getMaterial() {
        return material;
    }

    public int getData() {
        return data;
    }

    public List<String> getNames() {
        return names;
    }

    public void addName(String name) {
        if (!names.contains(name)) names.add(name);
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public double getDonatorBuy() {
        return donatorBuy;
    }

    public void setDonatorBuy(double donatorBuy) {
        this.donatorBuy = donatorBuy;
    }

    public double getDonatorSell() {
        return donatorSell;
    }

    public void setDonatorSell(double donatorSell) {
        this.donatorSell = donatorSell;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getDonatorAmount() {
        return donatorAmount;
    }

    public void setDonatorAmount(int donatorAmount) {
        this.donatorAmount = donatorAmount;
    }
}

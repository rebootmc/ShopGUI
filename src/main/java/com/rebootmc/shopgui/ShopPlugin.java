package com.rebootmc.shopgui;

import com.rebootmc.shopgui.api.menu.InventoryClickType;
import com.rebootmc.shopgui.api.menu.Menu;
import com.rebootmc.shopgui.api.menu.MenuAPI;
import com.rebootmc.shopgui.api.menu.MenuItem;
import com.rebootmc.shopgui.multiplier.Multiplier;
import com.rebootmc.shopgui.multiplier.MultiplierCommand;
import com.rebootmc.shopgui.multiplier.MultiplierManager;
import com.rebootmc.shopgui.util.ItemUtil;
import com.rebootmc.shopgui.util.TextUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class ShopPlugin extends JavaPlugin {

    /*
     * The shop menu
     */
    private Menu shop;
    private Menu dshop;

    /*
     * The economy implementation
     */
    private Economy economy;
    private List<ShopItem> items;
    private static ShopPlugin instance;

    public static ShopPlugin get() {
        return instance;
    }

    private static MultiplierManager manager;

    public static MultiplierManager getManager() {
        return manager;
    }

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        economy = Bukkit.getServicesManager().getRegistration(Economy.class).getProvider();
        getServer().getPluginManager().registerEvents(MenuAPI.getMenuAPI(), this);
        items = new ArrayList<>();
        manager = new MultiplierManager();

        try {
            loadItemsFromDB();
        } catch (IOException e) {
            getLogger().severe("Failed to load item aliases from items.txt!");
            e.printStackTrace();
        }

        this.shop = createShop(getConfig().getConfigurationSection("shop"));
        this.dshop = createShop(getConfig().getConfigurationSection("dshop"));

        SellCommand sellCmd = new SellCommand();
        getCommand("sell").setExecutor(sellCmd);
        getCommand("dsell").setExecutor(sellCmd);

        BuyCommand buyCmd = new BuyCommand();
        getCommand("buy").setExecutor(buyCmd);
        getCommand("dbuy").setExecutor(buyCmd);

        getCommand("multiplier").setExecutor(new MultiplierCommand());
    }

    @Override
    public void onDisable() {
        manager.save();
    }

    private Menu createShop(ConfigurationSection shopConfiguration) {
        int index = 0;

        // Set up the shop menu
        int numSections = shopConfiguration.getKeys(false).size();
        Menu shop = new Menu(TextUtil.color(getConfig().getString("gui.title")), (int) Math.ceil(numSections / 9D));

        for (String section : shopConfiguration.getKeys(false)) {
            ItemStack icon = ItemUtil.createItemStack(Material.getMaterial(shopConfiguration.getString(section + ".icon")), 1, TextUtil.color(section), null);

            ConfigurationSection subSectionConfig = shopConfiguration.getConfigurationSection(section + ".items");
            int numItems = subSectionConfig.getKeys(false).size();
            final Menu subSection = new Menu(ChatColor.DARK_GRAY + section, (int) Math.ceil(numItems / 9D), shop);
            int subMenuIndex = 0;

            for (String item : subSectionConfig.getKeys(false)) {
                //String[] itemSplit = item.toUpperCase().split(":");

                Material mat = Material.getMaterial(subSectionConfig.getString(item + ".data.material"));
                short durability = (short) subSectionConfig.getInt(item + ".data.durability", 0);
                String name = subSectionConfig.getString(item + ".data.name");

                final ItemStack itemIcon = ItemUtil.createItemStack(mat, 1, durability, name, null);
                final int buyAmount = subSectionConfig.getInt(item + ".buy.amount", 0);
                final double buyPrice = subSectionConfig.getDouble(item + ".buy.price", -1);

                final int sellAmount = subSectionConfig.getInt(item + ".sell.amount", -1);
                final double sellPrice = subSectionConfig.getDouble(item + ".sell.price", -1);
                final int sellStackAmount = subSectionConfig.getInt(item + ".sell.stack-amount", -1);
                final double sellStackPrice = (sellStackAmount / sellAmount) * sellPrice;

                ShopItem shopItem = getShopItem(mat, (int) durability);
                if (shopItem != null) {
                    if (shopConfiguration.getName().equalsIgnoreCase("dshop")) {
                        shopItem.setDonatorBuy(buyPrice);
                        shopItem.setDonatorSell(sellPrice);
                        shopItem.setDonatorAmount(buyAmount);
                    } else {
                        shopItem.setBuy(buyPrice);
                        shopItem.setSell(sellPrice);
                        shopItem.setAmount(buyAmount);
                    }
                }

                // Lore for the item
                List<String> iconLore = subSectionConfig.isList(item + ".data.lore") ? subSectionConfig.getStringList(item + ".data.lore") : new ArrayList<String>();
                if (!iconLore.isEmpty()) {
                    iconLore.add(" ");
                }

                if (buyAmount > -1) {
                    double price = manager.isActive() ? buyPrice * manager.getMultiplier().getMultiplier() : buyPrice;
                    iconLore.add(getFormattedConfigMessage("gui.buy-text", price, buyAmount == 0 ? 1 : buyAmount));
                }

                if (sellAmount > 0) {
                    double price = manager.isActive() ? sellPrice * manager.getMultiplier().getMultiplier() : sellPrice;
                    iconLore.add(TextUtil.color(getFormattedConfigMessage("gui.sell-text", price, sellAmount)));

                    if (sellStackAmount > 0 && sellStackAmount != sellAmount) {
                        double stackPrice = manager.isActive() ? sellStackPrice * manager.getMultiplier().getMultiplier() : sellStackPrice;
                        iconLore.add(TextUtil.color(getFormattedConfigMessage("gui.sell-stack-text", stackPrice, sellStackAmount)));
                    }
                }

                if (manager.isActive()) {
                    iconLore.add(TextUtil.color(getFormattedConfigMessage("gui.multiplier-text", manager.getMultiplier().getMultiplier())));
                }

                if (!iconLore.isEmpty()) {
                    ItemUtil.setLore(itemIcon, iconLore);
                }

                final List<String> buyCommands = subSectionConfig.getStringList(item + ".data.buy-commands");
                final List<String> sellCommands = subSectionConfig.getStringList(item + ".data.sell-commands");

                // Add item thingy
                subSection.addMenuItem(new MenuItem(itemIcon) {
                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        boolean isBuy = clickType.isLeftClick();

                        if (clickType.isShiftClick()) { // Sell Stack Item
                            isBuy = false; // If can't sell by stack, default to sell by amount

                            if (sellStackAmount > 0 && sellStackAmount != sellAmount) {
                                playerSellItem(player, itemIcon, sellStackPrice, sellStackAmount, sellCommands, true);
                                return;
                            }
                        }

                        if (isBuy) { // Buy Item
                            playerBuyItem(player, itemIcon, buyPrice, buyAmount, buyCommands);
                        } else if (sellAmount > 0) { // Sell Item
                            playerSellItem(player, itemIcon, sellPrice, sellAmount, sellCommands, true);
                        }
                    }
                }, subMenuIndex % 9, (int) Math.floor(subMenuIndex++ / 9D));
            }

            // Add the sub section menu
            shop.addMenuItem(new MenuItem(icon) {
                @Override
                public void onClick(Player player, InventoryClickType clickType) {
                    getMenu().switchMenu(player, subSection);
                }
            }, index % 9, (int) Math.floor(index++ / 9D));

            // Open parent on close
            subSection.setMenuCloseBehaviour(new MenuAPI.MenuCloseBehaviour() {
                @Override
                public void onClose(Player player, Menu menu, boolean bypassMenuCloseBehaviour) {
                    if (bypassMenuCloseBehaviour)
                        return;

                    menu.switchMenu(player, menu.getParent());
                }
            });
        }

        return shop;
    }

    /*
     * Issue list of commands and replace %player% with players name
     */
    private void issueCommands(Player player, List<String> commands) {
        for (String cmd : commands) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player.getName()));
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("shop.reload")) {
                sender.sendMessage(ChatColor.RED + "Insufficient permissions.");
            } else {
                reload();
                sender.sendMessage(ChatColor.GREEN + "Shop reloaded");
            }
        } else if (!(sender instanceof Player)) {
            sender.sendMessage("Only players can open the shop.");

        } else if (command.getName().equalsIgnoreCase("shop")) {
            if (!sender.hasPermission("shopgui.shop")) {
                sender.sendMessage(ChatColor.RED + "Insufficient permissions.");
            } else {
                Player player = (Player) sender;
                shop.openMenu(player);
            }
        } else if (command.getName().equalsIgnoreCase("dshop")) {
            if (!sender.hasPermission("shopgui.dshop")) {
                sender.sendMessage(ChatColor.RED + "Insufficient permissions.");

            } else {
                Player player = (Player) sender;
                dshop.openMenu(player);
            }
        }

        return true;
    }

    public void reload() {
        for (final HumanEntity ent : new ArrayList<>(shop.getInventory().getViewers())) {
            ent.closeInventory();
            ent.sendMessage(ChatColor.RED + "The shop is reloading..");
        }
        for (final HumanEntity ent : new ArrayList<>(dshop.getInventory().getViewers())) {
            ent.closeInventory();
            ent.sendMessage(ChatColor.RED + "The shop is reloading..");
        }

        try {
            reloadConfig();
            this.shop = createShop(getConfig().getConfigurationSection("shop"));
            this.dshop = createShop(getConfig().getConfigurationSection("dshop"));

        } catch (Exception ex) {
            getLogger().severe("Failed to reload the shop! Is the config defined correctly?");
        }
    }

    /*
     * Handles a Player selling an Item
     */
    public void playerSellItem(Player player, ItemStack item, double price, int amount, List<String> commands, boolean message) {
        //ItemStack noMeta = ItemUtil.clearItemMeta(item.clone());
        ItemStack noMeta = new ItemStack(item.getType(), 1, item.getDurability());
        noMeta.setAmount(amount);

        price = manager.isActive() ? price * manager.getMultiplier().getMultiplier() : price;

        if (!ItemUtil.has(player.getInventory(), noMeta, amount)) {
            player.sendMessage(getFormattedConfigMessage("messages.player-not-enough-items"));
        } else {
            economy.depositPlayer(player, price);
            player.getInventory().removeItem(noMeta);
            if (commands != null && !commands.isEmpty()) {
                issueCommands(player, commands);
            }
            if (message) {
                player.sendMessage(getMsg("divider"));

                player.sendMessage(getFormattedConfigMessage("messages.player-sold-item", amount, TextUtil.getFriendlyName(item.getType().toString()), price, TextUtil.formatDouble(economy.getBalance(player))));
                if (manager.isActive()) {
                    Multiplier m = manager.getMultiplier();
                    player.sendMessage(TextUtil.color(getConfig().getString("messages.multiplier-info")
                            .replace("{name}", m.getName())
                            .replace("{multiplier}", String.valueOf(m.getMultiplier()))
                            .replace("{time}", m.getRemaining())));
                }

                player.sendMessage(getMsg("divider"));
            }
        }
    }

    /*
     * Handles a Player buying an Item
     */
    public void playerBuyItem(Player player, ItemStack item, double price, int amount, List<String> commands) {
        ItemStack noMeta = ItemUtil.clearItemMeta(item.clone());
        noMeta.setAmount(amount);

        price = manager.isActive() ? price * manager.getMultiplier().getMultiplier() : price;

        if (!economy.has(player, price)) {
            player.sendMessage(getFormattedConfigMessage("messages.player-not-enough-money"));
        } else {
            economy.withdrawPlayer(player, price);

            player.sendMessage(getMsg("divider"));
            if (amount > 0) {
                boolean dropped = ItemUtil.giveItem(player, noMeta, amount);

                player.sendMessage(getFormattedConfigMessage("messages.player-bought-item", amount, TextUtil.getFriendlyName(item.getType().toString()), price, TextUtil.formatDouble(economy.getBalance(player))));
                if (dropped) {
                    player.sendMessage(getFormattedConfigMessage("messages.player-inventory-full"));
                }
            }
            if (commands != null && !commands.isEmpty()) {
                issueCommands(player, commands);
            }

            if (manager.isActive()) {
                Multiplier m = manager.getMultiplier();
                player.sendMessage(TextUtil.color(getConfig().getString("messages.multiplier-info")
                        .replace("{name}", m.getName())
                        .replace("{multiplier}", String.valueOf(m.getMultiplier()))
                        .replace("{time}", m.getRemaining())));
            }

            player.sendMessage(getMsg("divider"));
        }
    }

    public void loadItemsFromDB() throws IOException {
        ShopPlugin plugin = ShopPlugin.get();
        File file = new File(plugin.getDataFolder(), "items.txt");
        if (!file.exists()) {
            plugin.getLogger().info("No items.txt found, not loading name aliases!");
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] split = line.split(",");

                int id, data;
                try {
                    id = Integer.parseInt(split[1]);
                    data = split.length == 3 ? Integer.parseInt(split[2]) : 0;
                } catch (NumberFormatException ex) {
                    continue;
                }

                Material material = Material.getMaterial(id);
                ShopItem item = getShopItem(material, data);

                if (item != null) {
                    item.addName(split[0]);
                } else {
                    ShopItem i = new ShopItem(material, data, new ArrayList<String>());
                    i.addName(split[0]);
                    items.add(i);
                }
            }
        }
    }

    public ShopItem getShopItem(Material material, int data) {
        for (ShopItem item : items) {
            if (item.getMaterial() == material && item.getData() == data) return item;
        }

        return null;
    }

    public ShopItem getFromAlias(String s) {
        for (ShopItem item : items) {
            if (item.getMaterial() != null && item.getMaterial().name().equalsIgnoreCase(s)) return item;
            if (item.getNames().contains(s.toLowerCase())) return item;
        }

        return null;
    }

    public String getFormattedConfigMessage(String path, Object... objects) {
        return TextUtil.color(MessageFormat.format(getConfig().getString(path), objects));
    }

    public String getMsg(String path) {
        return TextUtil.color(getConfig().getString("messages." + path));
    }

    public ItemStack attemptItem(String s) {
        Material material = Material.getMaterial(s);
        ShopItem shopItem = null;

        if (material == null) {
            shopItem = ShopPlugin.get().getFromAlias(s);
            if (shopItem == null) return null;
            material = shopItem.getMaterial();
        }

        if (material == null) return null;

        return new ItemStack(material, 1, (short) (shopItem != null ? shopItem.getData() : 0));
    }
}

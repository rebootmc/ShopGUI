package com.rebootmc.shopgui;

import com.rebootmc.shopgui.util.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;

import java.util.ArrayList;

public class BuyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(TextUtil.color("&cYou must be a player to perform this command!"));
            return true;
        }

        ShopPlugin plugin = ShopPlugin.get();
        Player player = (Player) sender;
        boolean isDonator = cmd.getName().toLowerCase().startsWith("d");

        if((isDonator && !player.hasPermission("shopgui.dbuy") || (!isDonator && !player.hasPermission("shopgui.buy")))) {
            player.sendMessage(TextUtil.color("&cYou do not have permission to perform this command!"));
            return true;
        }

        if (args.length == 0) {
            player.sendMessage(TextUtil.color("&cInvalid arguments! Usage: /" + cmd.getName() + " <item> [amount]"));
            return true;
        }

        ItemStack item = plugin.attemptItem(args[0]);
        if (item == null) {
            player.sendMessage(TextUtil.color("&cFailed to find an item with that name!"));
            return true;
        }

        int amount = 1;
        if (args.length > 1) {
            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException ex) {
                player.sendMessage(TextUtil.color("&cInvalid amount specified, please enter a number!"));
                return true;
            }
        }

        ShopItem shopItem = plugin.getShopItem(item.getType(), item.getDurability());
        if (shopItem == null || (isDonator && shopItem.getDonatorBuy() <= 0) || (!isDonator && shopItem.getBuy() <= 0)) {
            player.sendMessage(TextUtil.color("&cYou cannot buy this item from the store!"));
            return true;
        }
        
        if(amount < 1 || amount > 2304) {
            player.sendMessage(TextUtil.color("&cPlease enter a valid amount!"));
            return true;
        }
        
        int items = 0;
        for(ItemStack itemA : player.getInventory())
            if(itemA == null || itemA.getType() == Material.AIR)
                items = items + 64;
        
        if(amount > items) {
            player.sendMessage(TextUtil.color("&cNot enough inventory space!"));
            return true;
        }

        double price = normalizePrice(shopItem, isDonator) * amount;
        plugin.playerBuyItem(player, item, price, amount, new ArrayList<String>());

        return false;
    }

    public double normalizePrice(ShopItem item, boolean isDonator) {
        double price = isDonator ? item.getDonatorBuy() : item.getBuy();
        int stackAmount = isDonator ? item.getDonatorAmount() : item.getAmount();

        if (stackAmount > 1) {
            return price / stackAmount;
        } else return price;
    }
}

package com.rebootmc.shopgui.multiplier;

import com.rebootmc.shopgui.ShopPlugin;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MultiplierManager {

    private Multiplier multiplier; // The currently active multiplier.
    private List<Multiplier> queue = new ArrayList<>(); // The queue of multipliers waiting to be started.
    private FileConfiguration config = ShopPlugin.get().getConfig();

    public MultiplierManager() {
        for (String s : config.getStringList("saved-queue")) {
            Multiplier m = deserialize(s);
            if (m == null) continue;

            queue.add(m);
        }
    }

    // Save the queued multipliers upon disable.
    public void save() {
        List<String> list = queue.stream().map(Multiplier::serialize).collect(Collectors.toList());

        config.set("saved-queue", list);
        ShopPlugin.get().saveConfig();
    }

    // Returns true if multiplier starts now, false if it enters the queue.
    public boolean addMultiplier(Multiplier multiplier) {
        if (isActive()) {
            queue.add(multiplier);

            return false;
        } else {
            this.multiplier = multiplier;
            this.multiplier.start();

            return true;
        }
    }

    // Gets the first value in the queue and starts it.
    public void processQueue() {
        if (!queue.isEmpty()) {
            this.multiplier = queue.get(0);
            this.multiplier.start();
            queue.remove(this.multiplier);
        }
    }

    // Returns true if the multiplier is not null and currently active.
    public boolean isActive() {
        return multiplier != null && multiplier.isActive();
    }

    // Save a multiplier as a string.
    public Multiplier deserialize(String s) {
        String[] split = s.split(":");

        return new Multiplier(split[0], Double.parseDouble(split[1]), Double.parseDouble(split[2]));
    }

    // Returns the currently active (or previously active) multiplier object.
    public Multiplier getMultiplier() {
        return multiplier;
    }

    // Returns the queue of multipliers.
    public List<Multiplier> getQueue() {
        return queue;
    }
}

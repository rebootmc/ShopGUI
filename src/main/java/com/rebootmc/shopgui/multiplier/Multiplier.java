package com.rebootmc.shopgui.multiplier;

import com.rebootmc.shopgui.ShopPlugin;
import com.rebootmc.shopgui.util.TextUtil;
import org.bukkit.Bukkit;

public class Multiplier {

    private double multiplier, duration;
    private int timeLeft;
    private boolean active;
    private String name;

    public Multiplier(String name, double multiplier, double duration) {
        this.name = name;
        this.multiplier = multiplier;
        this.duration = duration;
        this.timeLeft = (int) duration * 60;
    }

    public void start() {
        final ShopPlugin plugin = ShopPlugin.get();

        Bukkit.broadcastMessage(TextUtil.color(plugin.getConfig()
                .getString("messages.multiplier-broadcast")
                .replace("{name}", name).replace("{multiplier}", String.valueOf(multiplier)).replace("{time}", getTime(duration))));

        active = true;
        plugin.reload();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (timeLeft > 0) timeLeft--;

            if (active && timeLeft == 0) {
                if (active) {
                    Bukkit.broadcastMessage(TextUtil.color(plugin.getConfig().getString("messages.multiplier-finished")
                            .replace("{name}", name).replace("{multiplier}", String.valueOf(multiplier))));
                }

                active = false;
                plugin.reload();

                // After 5 seconds, see if there is another multiplier in the queue to start.
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> ShopPlugin.getManager().processQueue(), 20 * 5);
            }
        }, 0, 20);
    }

    public String serialize() {
        return name + ":" + multiplier + ":" + duration;
    }

    // Basic time formatting utility.
    public String getTime(double time) {
        if (time < 1) {
            return TextUtil.formatDouble(time * 60) + " seconds";
        } else return time + " minutes";
    }

    public String getRemaining() {
        if (timeLeft < 60) {
            return timeLeft + " seconds";
        } else return TextUtil.formatDouble(((double) timeLeft) / 60) + " minutes";
    }

    public double getMultiplier() {
        return multiplier;
    }

    public boolean isActive() {
        return active;
    }

    public String getName() {
        return name;
    }

    public double getTime() {
        return duration;
    }
}

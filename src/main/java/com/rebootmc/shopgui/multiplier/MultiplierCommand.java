package com.rebootmc.shopgui.multiplier;

import com.rebootmc.shopgui.ShopPlugin;
import com.rebootmc.shopgui.util.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;

public class MultiplierCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        MultiplierManager manager = ShopPlugin.getManager();
        ShopPlugin plugin = ShopPlugin.get();

        if (args.length > 0 && args[0].equalsIgnoreCase("list")) {
            if (manager.isActive()) {
                Multiplier m = manager.getMultiplier();
                sender.sendMessage(plugin.getMsg("multiplier-active").replace("{multiplier}", "" + m.getMultiplier())
                        .replace("{name}", m.getName()).replace("{time}", m.getRemaining()));

                List<Multiplier> queue = manager.getQueue();
                if (!queue.isEmpty()) {
                    sender.sendMessage(plugin.getMsg("multiplier-list-title").replace("{amount}", "" + queue.size()));

                    for (Multiplier mp : queue) {
                        sender.sendMessage(plugin.getMsg("multiplier-list-value")
                                .replace("{name}", mp.getName()).replace("{multiplier}", "" + mp.getMultiplier()).replace("{time}", mp.getTime(mp.getTime())));
                    }
                }

            } else sender.sendMessage(TextUtil.color("&cThere are no active or queued shop multipliers at this time!"));

            return true;
        }
        if (!sender.hasPermission("multiplier.cmd")) {
            sender.sendMessage(TextUtil.color("&cYou do not have permission to use this command!"));
            return true;
        }

        if (args.length < 2) {
            sender.sendMessage(TextUtil.color("&cInvalid arguments! Usage: /multiplier <multiplier> <duration in minutes>"));
            return true;
        }

        double multiplier, duration;
        try {
            multiplier = Double.parseDouble(args[0]);
            duration = Double.parseDouble(args[1]);
        } catch (NumberFormatException ex) {
            sender.sendMessage(TextUtil.color("&cInvalid values specified, please enter a number!"));
            return true;
        }

        boolean started = manager.addMultiplier(new Multiplier(sender.getName(), multiplier, duration));
        sender.sendMessage(TextUtil.color(started ?
                "&7You've successfully initiated a " + multiplier + "x multiplier for " + duration + " minutes" :
                "&7Your multiplier has been added to the queue. Use /multiplier list to view the queue."));

        return false;
    }
}

package com.rebootmc.shopgui;

import com.rebootmc.shopgui.multiplier.Multiplier;
import com.rebootmc.shopgui.multiplier.MultiplierManager;
import com.rebootmc.shopgui.util.TextUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class SellCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(TextUtil.color("&cYou must be a player to perform this command!"));
            return true;
        }

        ShopPlugin plugin = ShopPlugin.get();
        MultiplierManager manager = ShopPlugin.getManager();
        Player player = (Player) sender;
        boolean isDonator = cmd.getName().toLowerCase().startsWith("d");

        if ((isDonator && !player.hasPermission("shopgui.dshop")) || (!isDonator && !player.hasPermission("shopgui.shop"))) {
            player.sendMessage(TextUtil.color("&cYou do not have permission to perform this command!"));
            return true;
        }

        if (args.length == 0) {
            player.sendMessage(TextUtil.color("&cInvalid arguments! Usage: /" + cmd.getName() + " <item:hand:inv:all> [amount]"));
            return true;
        }

        // Sell the item in the player's hand.
        if (args[0].equalsIgnoreCase("hand")) {
            if((isDonator && !player.hasPermission("shopgui.dsell.hand") || (!isDonator && !player.hasPermission("shopgui.sell.hand")))) {
                player.sendMessage(TextUtil.color("&cYou do not have permission to perform this command!"));
                return true;
            }

            ItemStack hand = player.getItemInHand();
            ShopItem shopItem = plugin.getShopItem(hand.getType(), hand.getDurability());

            if (hand.getType() == Material.AIR) {
                player.sendMessage(TextUtil.color("&cYou have nothing in your hand!"));
                return true;
            }

            if (shopItem == null || !canSell(shopItem, isDonator)) {
                player.sendMessage(TextUtil.color("&cThis item cannot be sold to the shop!"));
                return true;
            }

            plugin.playerSellItem(player, hand, (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * hand.getAmount(), hand.getAmount(), new ArrayList<String>(), true);

            return true;
        }

        // Attempt to sell every item in the player's inventory.
        if (args[0].equalsIgnoreCase("all")) {
            if((isDonator && !player.hasPermission("shopgui.dsell.all") || (!isDonator && !player.hasPermission("shopgui.sell.all")))) {
                player.sendMessage(TextUtil.color("&cYou do not have permission to perform this command!"));
                return true;
            }

            int successful = 0;
            double price = 0;
            for (ItemStack i : player.getInventory().getContents()) {
                if (i == null || i.getType() == Material.AIR) continue;

                ShopItem shopItem = plugin.getShopItem(i.getType(), i.getDurability());
                if (shopItem == null || !canSell(shopItem, isDonator)) continue;

                price = price + (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * i.getAmount();

                plugin.playerSellItem(player, i, (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * i.getAmount(), i.getAmount(), new ArrayList<String>(), false);
                successful++;
            }

            player.sendMessage(plugin.getMsg("divider"));
            player.sendMessage(plugin.getMsg("sell-all").replace("{amount}", "" + successful).replace("{price}", "" + Math.round(price)));

            if (manager.isActive()) {
                Multiplier m = manager.getMultiplier();
                player.sendMessage(TextUtil.color(plugin.getConfig().getString("messages.multiplier-info")
                        .replace("{name}", m.getName())
                        .replace("{multiplier}", String.valueOf(m.getMultiplier()))
                        .replace("{time}", m.getRemaining())));
            }

            player.sendMessage(plugin.getMsg("divider"));
            return true;
        }

        if(args[0].equalsIgnoreCase("inv")) {
            if((isDonator && !player.hasPermission("shopgui.dsell.inv") || (!isDonator && !player.hasPermission("shopgui.sell.inv")))) {
                player.sendMessage(TextUtil.color("&cYou do not have permission to perform this command!"));
                return true;
            }

            ItemStack hand = player.getItemInHand();
            ShopItem shopItem = plugin.getShopItem(hand.getType(), hand.getDurability());

            if (hand.getType() == Material.AIR) {
                player.sendMessage(TextUtil.color("&cYou have nothing in your hand!"));
                return true;
            }

            if (shopItem == null || !canSell(shopItem, isDonator)) {
                player.sendMessage(TextUtil.color("&cThis item cannot be sold to the shop!"));
                return true;
            }

            int successful = 0;
            double price = 0;
            for(ItemStack item : player.getInventory()) {
                if(item != null)
                    if(item.getType() == hand.getType() && item.getDurability() == hand.getDurability()) {
                        price = price + (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * item.getAmount();
                        plugin.playerSellItem(player, item, (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * item.getAmount(), item.getAmount(), new ArrayList<>(), false);
                        successful++;
                    }
            }

            player.sendMessage(plugin.getMsg("divider"));
            player.sendMessage(plugin.getMsg("sell-all").replace("{amount}", "" + successful).replace("{price}", "" + Math.round(price)));

            if (manager.isActive()) {
                Multiplier m = manager.getMultiplier();
                player.sendMessage(TextUtil.color(plugin.getConfig().getString("messages.multiplier-info")
                        .replace("{name}", m.getName())
                        .replace("{multiplier}", String.valueOf(m.getMultiplier()))
                        .replace("{time}", m.getRemaining())));
            }

            player.sendMessage(plugin.getMsg("divider"));
            return true;
        }

        ItemStack item = plugin.attemptItem(args[0]);
        if (item == null) {
            player.sendMessage(TextUtil.color("&cFailed to find an item with that name!"));
            return true;
        }

        int amount = 1;
        if (args.length > 1) {
            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException ex) {
                player.sendMessage(TextUtil.color("&cInvalid amount specified, please enter a number!"));
                return true;
            }
        }
        
        if(amount < 1 || amount > 2304) {
            player.sendMessage(TextUtil.color("&cPlease enter a valid amount!"));
            return true;
        }
        
        int itemAmount = 0;
        for(ItemStack items : player.getInventory())
            if(items != null)
                if(items.getType() == item.getType())
                    itemAmount = itemAmount + items.getAmount();
                
        if(itemAmount < amount) {
            player.sendMessage(TextUtil.color("&cPlease enter a valid amount!"));
            return true;
        }

        ShopItem shopItem = plugin.getShopItem(item.getType(), item.getDurability());
        double price = (isDonator ? shopItem.getDonatorSell() : shopItem.getSell()) * amount;
        plugin.playerSellItem(player, item, price, amount, new ArrayList<String>(), true);

        return false;
    }

    public boolean canSell(ShopItem item, boolean isDonator) {
        return item != null && !(!isDonator && (item.getSell() <= 0)) && !(isDonator && (item.getDonatorSell() <= 0));
    }
}
